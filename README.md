CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Developers
 * Maintainers

INTRODUCTION
------------
This module provides a way to show a message on a form
when editing something and leaving without submitting.

Based on https://www.drupal.org/project/node_edit_protection
But allows you to add the form alert on any form.

Leave site?
Changes that you made may not be saved.

**
 * Implements hook_form_alter().
 */
function demo_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  modified_form_alert_apply($form);
}

REQUIREMENTS
------------
- php >= 7.1
- Drupal >= 8.5

INSTALLATION
------------
Install the "Modified form alert" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------
In a hook_form_alter(), apply the modified form alert
by calling the function  modified_form_alert_apply(array &$form).

DEVELOPERS
----------
* Maarten Heip - https://www.drupal.org/u/mheip

But a special thanks to Intracto, for allowing me to do this on company time.
https://www.intracto.com

MAINTAINERS
-----------
Current maintainers:
  * Maarten Heip - https://www.drupal.org/u/mheip
