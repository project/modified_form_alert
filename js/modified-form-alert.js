/**
 * @file
 * Stops page from changing when a form element has finished loading.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.modifiedFormAlert = {
    attach: function (context) {
      var self = this;

      once('modified-form-alert', '.modified-form-alert', context)
        .forEach( function () {
          self.showAlert = false;
          self.attachListeners();
      });

      // Re-attach event listeners when ajax has loaded a part of the form.
      $(context).ajaxComplete(function () {
        self.attachListeners();
      });
    },

    /**
     * Attaches the listeners to our events.
     */
    attachListeners: function () {
      var self = this;

      // When an input field has changed, set the show alert variable.
      $('.modified-form-alert').change(function (e) {
        self.edited = true;
      });

      // When the form submits, do not show an alert.
      $('form.modified-form-alert').submit(function () {
        self.submitting = true;
      });

      // When the page unloads, return a popup message.
      $(window).on('beforeunload', function () {
        if (self.submitting !== true) {
          if (!self.edited) {
            // Check CKEditor instances for wysiwyg changes.
            for (var i in CKEDITOR.instances) {
              if (CKEDITOR.instances[i].checkDirty()) {
                self.edited = true;
                return Drupal.t('You are about to leave the page, any work you have done will be for nothing!');
              }
            }
          }
          else {
            return Drupal.t('You are about to leave the page, any work you have done will be for nothing!');
          }
        }
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
